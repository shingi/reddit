package main

import (
	"fmt"
	"log"

	"bitbucket.org/shingi/reddit"
)

func main() {
	items, err := reddit.Get("golang")

	if err != nil {
		log.Fatal(err)
	}

	// ignore the index
	for _, item := range items {
		fmt.Println(item)
	}
}
